
import numpy as np
import os
from collections import Counter
from collections import namedtuple
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer

emailSet = namedtuple("emailSet", ["content", "status"])

dir = 'dataset'

def getText(cv, content):
    split = cv.inverse_transform(content)
    print(split)
    # print(split.tolist())
    return " ".join(split)

def getData():
    emailFiles = [os.path.join(dir,f) for f in os.listdir(dir)]
    emails = []
    # each file in dir
    for mail in emailFiles:
        # open file
        with open(mail) as m:
            # for each email
            for line in m:
                # email splitted into words
                words = line.split()

                # file format-specific parsing
                content = " ".join(words[1:])
                status = words[0]
                emails.append(emailSet(content, status))

    cv = TfidfVectorizer(min_df=1, stop_words='english') # TF-IDF
    # cv = CountVectorizer() # Frequency count

    # get TF-IDF
    emails_content = [x.content for x in emails]
    train = cv.fit_transform(emails_content)
    # print(emails_content)

    # set returns
    content = train.toarray()
    status = ([1,0] if x.status == "spam" else [0,1] for x in emails)

    return content, status, cv
