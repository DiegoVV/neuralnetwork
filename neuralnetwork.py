import numpy as np
import time

DEBUG = 0

def sigmoid (x):
	return 1 / (1 + np.exp(-x))

def sigmoidPrime(x):
	return x * (1 - x)

def ReLu(x):
	return np.maximum(0,x)

def ReLuPrime(x):
	if x>0:
		return 1
	else:
		return 0

class NeuralNetwork(object):

	def __init__(self, inputNodesNum, hiddenNodesNum, outputNodesNum, learningRate):
		self.inputNodesNum = inputNodesNum
		self.hiddenNodesNum = hiddenNodesNum
		self.outputNodesNum = outputNodesNum
		self.learningRate = learningRate
		self.inputBias = np.random.rand(hiddenNodesNum,1)
		self.hiddenBias = np.random.rand(outputNodesNum,1)
		if(DEBUG):
			print("===============")
			print("InputBias:")
			print(self.inputBias.shape)
			print("HiddenBias:")
			print(self.hiddenBias.shape)

		self.inputWeights = np.random.rand(hiddenNodesNum, inputNodesNum)
		self.hiddenWeights = np.random.rand(outputNodesNum, hiddenNodesNum)

	def predict(self, inputs):
		inputs = np.array([inputs]).T
		hiddenValues = self.forward(self.inputWeights, inputs, self.inputBias)
		outputValues = self.forward(self.hiddenWeights, hiddenValues, self.hiddenBias)

		if(DEBUG):
			print("===============")
			print("Input:")
			print(inputs)
			print("Output:")
			print(outputValues)

		return outputValues

	def forward(self, previousLayerWeights, previousLayer, bias):
		nextLayerValues = np.dot(previousLayerWeights, previousLayer) + bias
		return sigmoid(nextLayerValues)

	def delta(self, previousLayer, nextLayer, errors):
		gradients = self.learningRate * errors * sigmoidPrime(nextLayer)
		deltas = np.dot(gradients, previousLayer.T)
		return gradients, deltas

	def train(self, inputs, targets):
		hiddenValues = self.forward(self.inputWeights, inputs, self.inputBias)
		outputValues = self.forward(self.hiddenWeights, hiddenValues, self.hiddenBias)
		errors = targets - outputValues

		# Hidden weights deltas
		hiddenWeightsGradients, hiddenWeightsDeltas = self.delta(hiddenValues, outputValues, errors)

		self.hiddenBias += hiddenWeightsGradients
		self.hiddenWeights += hiddenWeightsDeltas

		# Hidden layer errors
		hiddenErrors = np.dot(self.hiddenWeights.T, errors)

		# Inputs weights deltas
		inputWeightsGradients, inputWeightsDeltas = self.delta(inputs, hiddenValues, hiddenErrors)

		if(DEBUG):
			print("===============")
			print("HiddenGradients:")
			print(hiddenWeightsGradients.shape)
			print("InputGradients:")
			print(inputWeightsGradients.shape)

		self.inputBias += inputWeightsGradients
		self.inputWeights += inputWeightsDeltas

		if(DEBUG):
			print("\nInput:")
			print(inputs)
			print("Target:")
			print(targets)
			print("outputValues:")
			print(outputValues)
			print("errors:")
			print(errors)
			print("hiddenErrors:")
			print(hiddenErrors)
			print("inputWeightsDeltas:")
			print(inputWeightsDeltas)
			print("hiddenWeightsDeltas:")
			print(hiddenWeightsDeltas)
			time.sleep(2)
