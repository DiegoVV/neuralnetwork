
import numpy as np
import neuralnetwork
import extraction as ex
import time
import random

def percent(x, curr):
	return "{0}".format((int) (x / curr * 100))

learningRate = 0.1
DEBUG = 0

content, status, dataset = ex.getData()
zipped = list(zip(content, status))
random.shuffle(zipped)
content, status = zip(*zipped)

numInputs = len(content[0])
nn = neuralnetwork.NeuralNetwork(numInputs, 8, 2, learningRate)

trainPer = 0.5

numTrain = (int) (len(content) * trainPer)
numTest = len(content) - ((int) (len(content) * trainPer))
trainArray = content[0:numTrain]
trainStatus = status[0:numTrain]
testArray = content[numTrain:]
testStatus = status[numTrain:]

# testing
correctSpam = 0
Spam = 0
correctHam = 0
Ham = 0
for i in range(len(testArray)):
	inputs = testArray[i]
	targets = status[i]

	if(DEBUG):
		print("===============")
		print("Input:")
		print(inputs)
		print("Target:")
		print(targets)

	result = nn.predict(inputs)

	if(targets[0] == 1):
		Spam += 1
		# print("Target was 1, the result was {0}".format(result))
	else:
		Ham += 1

	# print(result)
	if (targets[0] == 1 and (result[0] > result[1])):
		#print("FOUND A SPAM! Neural Network predicted {0}".format(result))
		# ex.getText(dataset, inputs)
		correctSpam += 1
	if (targets[1] == 1 and (result[1] > result[0])):
		# print("Targets:")
		# print(targets)
		# print("Result:")
		# print(result)
		correctHam += 1

print("{0}/{1} Spam predictions were correct ({2}%)".format(correctSpam, Spam, percent(correctSpam,Spam)))
print("{0}/{1} Ham predictions were correct ({2}%)".format(correctHam, Ham, percent(correctHam,Ham)))
print("Total messages reviewed: {0}".format(len(testArray)))

# training
count = 0
for i in range(50000):
	random = np.random.randint(0, len(trainArray))
	inputs = np.array([trainArray[random]]).T
	targets = np.array([trainStatus[random]]).T
	# nn.train(inputs, targets)
	nn.train(inputs, targets)

	# if(targets == 0):
		# count += 1
	# if(count<10000 or targets == 1):
		# nn.train(inputs, targets)
	# else:
		# i -= 1

# testing
correctSpam = 0
Spam = 0
correctHam = 0
Ham = 0
for i in range(len(testArray)):
	inputs = testArray[i]
	targets = testStatus[i]

	if(DEBUG):
		print("===============")
		print("Input:")
		print(inputs)
		print("Target:")
		print(targets)

	result = nn.predict(inputs)

	if(targets[0] == 1):
		Spam += 1
		# print("Target was 1, the result was {0}".format(result))
	else:
		Ham += 1

	# print(result)
	if (targets[0] == 1 and (result[0] > result[1])):
		#print("FOUND A SPAM! Neural Network predicted {0}".format(result))
		correctSpam += 1
	if (targets[1] == 1 and (result[1] > result[0])):
		# print("Targets:")
		# print(targets)
		# print("Result:")
		# print(result)
		correctHam += 1

print("{0}/{1} Spam predictions were correct ({2}%)".format(correctSpam, Spam, percent(correctSpam,Spam)))
print("{0}/{1} Ham predictions were correct ({2}%)".format(correctHam, Ham, percent(correctHam,Ham)))
print("Total messages reviewed: {0}".format(len(testArray)))
